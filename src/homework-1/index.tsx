/* Core */
import { render } from 'react-dom';

/* Instruments */
import './theme/main.scss';

render(<h1>Hello world!</h1>, document.getElementById('root'));
